<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Pagination\Paginator;
use App\Models\Customer;

class Filter extends Component
{
	use WithPagination;

	public $searchTerm;
    public $currentPage = 1;

    public function render()
    {
    	$query = '%'.$this->searchTerm.'%';

    	return view('livewire.filter', [
    		'customers'		=>	Customer::where(function($sub_query){
    							$sub_query->where('date_of_shoot', 'like', '%'.$this->searchTerm.'%')
                                          ->orWhere('fname', 'like', '%'.$this->searchTerm.'%')
                                          ->orWhere('lname', 'like', '%'.$this->searchTerm.'%')
                                          ->orWhere('school', 'like', '%'.$this->searchTerm.'%')
                                          ->orWhere('course', 'like', '%'.$this->searchTerm.'%')
                                          ;
    						})->paginate(10)
    	]);
    }

    public function setPage($url)
    {
        $this->currentPage = explode('page=', $url)[1];
        Paginator::currentPageResolver(function(){
            return $this->currentPage;
        });
    }
}