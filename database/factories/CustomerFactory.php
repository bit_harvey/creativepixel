<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date_of_shoot' => now(),
            'student_id' => $this->faker->randomDigit,
            'fname' => $this->faker->name,
            'mname' => $this->faker->name,
            'lname' => $this->faker->name,
            'school' => $this->faker->name,
            'course' => $this->faker->name,
            'package' => $this->faker->name,
            'cnumber' => $this->faker->name,
            'payment' => $this->faker->name,
            'remarks' => $this->faker->name,
            'status' => $this->faker->name,
            'created_at' => now(),
            'updated_at' => now(),
            
            
        ];
    }
}
