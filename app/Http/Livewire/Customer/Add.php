<?php

namespace App\Http\Livewire\Customer;

use Livewire\Component;
use App\Models\Customer;


class Add extends Component
{
    public $date_of_shoot, $student_id, $fname, $lname, $mname, $school, $course, $package, $cnumber, $payment, $remarks, $status;

    public function render()
    {
        $this->customers = Customer::all();
        return view('livewire.customer.add');
    }
    private function resetInputFields(){
        $this->date_of_shoot = '';
        $this->student_id = '';
        $this->fname = '';
        $this->mname = '';
        $this->lname = '';
        $this->school = '';
        $this->course = '';
        $this->package = '';
        $this->cnumber = '';
        $this->payment = '';
        $this->remarks = '';
        $this->status = '';
    }

    public function store()
    {
        $validatedDate = $this->validate([
            'date_of_shoot' => 'required',
            'student_id' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'mname' => 'required',
            'school' => 'required',
            'course' => 'required',
            'package' => 'required',
            'cnumber' => 'required',
            'payment' => 'required',
            'remarks' => 'required',
            'status' => 'required',

        ]);

        Customer::create($validatedDate);

        session()->flash('message', 'Customer Created Successfully.');

        $this->resetInputFields();

    }
    public function edit($id)
    {
        $this->updateMode = true;
        $user = Customer::where('id',$id)->first();
        $this->id = $id;
        $this->date_of_shoot = $user->date_of_shoot;
        $this->student_id = $user->student_id;
        $this->fname = $user->fname;
        $this->mname = $user->mname;
        $this->lname = $user->lname;
        $this->school = $user->school;
        $this->course = $user->course;
        $this->package = $user->package;
        $this->cnumber = $user->cnumber;
        $this->payment = $user->payment;
        $this->remarks = $user->remarks;
        $this->status = $user->status;
        
    }
}



