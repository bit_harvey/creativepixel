<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Customer') }}
            <x-jet-button  class="float-right"><a href="{{ route('add') }}">{{ __('+ Add Customer') }}</a></x-jet-button> 
        </h2>
    </x-slot>

    <div class="py-12">

            @livewire('filter')
           
    </div>
    @livewireScripts
</x-app-layout>
