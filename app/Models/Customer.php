<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'date_of_shoot',
        'student_id',
        'fname',
        'mname',
        'lname',
        'school',
        'course',
        'package',
        'cnumber',
        'payment',
        'remarks',
        'status',

        
    ];
}
