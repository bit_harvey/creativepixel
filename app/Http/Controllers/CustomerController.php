<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {   
        return view('customer.index');
    }

    public function add_customer()
    {   
        return view('customer.add_customer');
    }
}
