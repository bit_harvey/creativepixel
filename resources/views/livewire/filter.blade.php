<div class="max-w-7xl mx-auto sm:px-6 lg:px-z">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="col-span-6 sm:col-span-4 p-5">
                    <x-jet-input type="text" class="mt-1 block w-full" placeholder="Quick Search" wire:model="searchTerm"  />
                </div>
            <div class="p-5">
            <table class="w-full flex flex-row flex-no-wrap sm:bg-white rounded-lg overflow-hidden sm:shadow-lg my-0">
			<thead class="text-white">
            @foreach($customers as $customer)
				<tr class="bg-teal-400 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
					<th class="p-3 text-left">Date of Shoot</th>
					<th class="p-3 text-left">First Name</th>
                    <th class="p-3 text-left">Last Name</th>
                    <th class="p-3 text-left">School</th>
                    <th class="p-3 text-left">Course</th>
                    <th class="p-3 text-left">Status</th>
					<th class="p-3 text-left" width="110px">Actions</th>
				</tr>
             @endforeach
			</thead>
			<tbody class="flex-1 sm:flex-none">
                @foreach($customers as $customer)
			        	<tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{ $customer->date_of_shoot }}</td>
                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{ $customer->fname }}</td>
                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{ $customer->lname }}</td>
                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{ $customer->school }}</td>
                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{ $customer->course }}</td>
                    <td class="border-grey-light border hover:bg-gray-100 p-3 truncate">{{ $customer->course }}</td>
                    <td class="border-grey-light border hover:bg-gray-100 p-3 text-blue-400 hover:text-blue-600 hover:font-medium cursor-pointer"><a href="">Open</a></td>
                </tr>
                @endforeach
			</tbody>
           
		</table>

        
            </div>
            <div class="p-5">
            {{ $customers->links('livewire.livewire-pagination') }}
            </div>
            </div>
        </div>


<style>
@media (min-width: 840px) {
    table {
      display: inline-table !important;
    }

    thead tr:not(:first-child) {
      display: none;
    }
  }

  td:not(:last-child) {
    border-bottom: 0;
  }

  th:not(:last-child) {
    border-bottom: 2px solid rgba(0, 0, 0, .1);
  }
</style>
</style>